# Deploy Heroku

- testing

- Pastikan sudah membuat akun heroku dan menginstall heroku-cli (cara cek heroku-cli, dengan mengetik heroku version pada terminal)
- Buat aplikasi melalui dashboard heroku
- Pastikan kodingan yang akan kalian deploy, memiliki .gitignore (mengignore node_modules)
- Pastikan di package.json terdapat script start: "node index.js"
- Pastikan portnya diganti dengan process.env.PORT

- heroku login
- git init (jika belum ada git di aplikasi)
- heroku git:remote -a binar-simple-app
- git add .
- git commit -m "messagenya apa"
- git push heroku master

- heroku addons:create heroku-postgresql:hobby-dev
- heroku run npx sequelize-cli db:migrate --env production --app binar-todo
- heroku run npx sequelize-cli db:seed:all --env production --app binar-todo
- heroku pg:psql (akses database)
